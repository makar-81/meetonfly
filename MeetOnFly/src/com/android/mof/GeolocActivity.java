package com.android.mof;

import ru.yandex.yandexmapkit.MapActivity;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import android.location.LocationManager;
import android.os.Bundle;
//import ru.yandex.yandexmapkit.R;


public class GeolocActivity extends MapActivity
{
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.geoloc_layout);
		
		MapView mMap = (MapView) findViewById(R.id.map);

		MapController mMapController = mMap.getMapController();
		LocationManager loc_manager = (LocationManager ) getSystemService(LOCATION_SERVICE);
		
        mMapController.setPositionAnimationTo(new   GeoPoint(60.113337, 55.151317)); 
        
	}
	
	
	protected void onDestroy(Bundle savedInstanceState) 
	{
		//TODO
		
	}
}