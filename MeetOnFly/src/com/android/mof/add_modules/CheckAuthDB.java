package com.android.mof.add_modules;

/*
*  This activity will check in database,
*  settings files if a user has been registered
*  before and it will return the result to
*  main activity
*     (c)2014 Evgeny Makarov
*/


//import com.android.mof.R;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
               



public class CheckAuthDB extends Activity 
{
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		/*Here we need to support a method of authorization checking
		 *from database and save it to local settings variable
		 *
		 */
		
		Intent data = new Intent();
		Uri phone_number = Uri.parse("tel:+79151055264");
		
		if (phone_number != null)
		   {
		        setResult(RESULT_OK,data);
		   }
		else 
		   {    
			    
			    setResult(RESULT_CANCELED,data);
		   }
			
		finish();
		
	}
	
}
