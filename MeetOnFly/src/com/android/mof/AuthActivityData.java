package com.android.mof;

import android.app.AlertDialog.Builder;

public class AuthActivityData {
	public String phone_num;
	public String fake_sms_code1;
	public String fake_sms_code2;
	public Builder ent_code;

	public AuthActivityData(String phone_num, String fake_sms_code1,
			String fake_sms_code2, Builder ent_code) {
		this.phone_num = phone_num;
		this.fake_sms_code1 = fake_sms_code1;
		this.fake_sms_code2 = fake_sms_code2;
		this.ent_code = ent_code;
	}
}