/*     
 *           (c)2014 Evgeny Makarov
 */

package com.android.mof;

import com.android.mof.R;
import com.android.mof.add_modules.CheckAuthDB;
import com.android.mof.add_modules.CheckSettings;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.content.Intent;
import android.content.res.Resources;
import android.widget.TextView;
//import android.util.Log;



public class MOFMainActivity extends Activity 
{
    
	/*
	 * Before Main Activity creation we check if
	 * the subscriber has been already registered
	 * before like in WhatsApp or Telegram
	 */
	
    boolean isAuth=false;
	
    int check_auth_activity=1,auth_activity=2,chat_activity=3,map_activity=4,contacts_activity=5,fav_activity=6,
		check_settings_activity=7;
	
    int stored_activity=fav_activity;
	

	//Local methods
	private boolean isAuthorized()
	{
		//startActivityForResult(new Intent(this,CheckAuthDB.class),check_auth_activity);
		return isAuth;
	}
	

	

	/*
	 *     Main Activities and methods
	 *     (c) 2014 Evgeny Makarov
	 */
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		/* Here before the authorization there need to check isAuthorized locally in the settings
		*  after that only start the SMS authorization. Create a method to check and upload the
		*  settings.
		*/
		
		//startActivityForResult(new Intent(this,CheckSettings.class),check_settings_activity);
		if (!isAuthorized()) startActivityForResult(new Intent(this,AuthActivity.class),auth_activity);
		
		TextView activity_name = (TextView) findViewById(R.id.ActivityName);
		activity_name.setText(getActivityName(stored_activity));
		
		
			
	}
	
	public void ChatOnClick (View v)
	{
		
		startActivityForResult(new Intent(this,ChatActivity.class),chat_activity);
	}
	
	public void MapOnClick (View v)
	{
		startActivityForResult(new Intent(this,GeolocActivity.class),map_activity);
	}
	
	public void FavoriteOnClick (View v)
	{
		startActivityForResult(new Intent(this,FavoriteActivity.class),fav_activity);
	}
	
	public void ContactsOnClick (View v)
	{
		startActivityForResult(new Intent(this,ContactsActivity.class),contacts_activity);
	}
	
	protected void onDestroy(Bundle savedInstanceState) 
	{

		super.onDestroy();
		//TODO
		/* 1.�� ���������� ���������,����� ���������� ���� ��������� � ���� ��������,
		     ���� �� ������� ������ ���!!!
		   2.��������,��� ��� ���������?
		   3.
		*/
	}

	
    /*
     * Additional Class Functions
     * 
     */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mofmain, menu);
		return true;
	}
	
	public void onActivityResult(int requestCode,int resultCode,Intent data) 
	{   
		if (requestCode==check_auth_activity)
		{
			if (resultCode==RESULT_OK)
			{
				isAuth=true;
			}
			//else  
		}
		
		if (requestCode==auth_activity)
		{
			if (resultCode==RESULT_OK)
			{
				isAuth=true;
			}
		   
		}
		
		if (requestCode==chat_activity)
		{
			if (resultCode==RESULT_OK)
			{
				//isAuth=true;
			}
		   
		}
		
		if (requestCode==map_activity)
		{
			if (resultCode==RESULT_OK)
			{
				//isAuth=true;
			}
		   
		}
		
		if (requestCode==contacts_activity)
		{
			if (resultCode==RESULT_OK)
			{
				//isAuth=true;
			}
		   
		}
		
		if (requestCode==fav_activity)
		{
			if (resultCode==RESULT_OK)
			{
				//isAuth=true;
			}
		   
		}
		
		if (requestCode==check_settings_activity)
		{
			if (resultCode==RESULT_OK)
			{
				//isAuth=true;
			}
		   
		}
	}
	
	protected String getActivityName (int name)
	{
		String ActivityName="";
		Resources activity_res = getResources();
		switch (name)
		{
		    case 3:ActivityName=activity_res.getString(R.string.main_chat_name_rus);break;
		    case 4:ActivityName=activity_res.getString(R.string.main_map_name_rus);break;
		    case 5:ActivityName=activity_res.getString(R.string.main_contacts_name_rus);break;
		    case 6:ActivityName=activity_res.getString(R.string.main_fav_name_rus);break;
		}
		
		
		return ActivityName;
	}
	
	
}
