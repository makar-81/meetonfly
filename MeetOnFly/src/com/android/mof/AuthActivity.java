package com.android.mof;

/*        This activity starts a user's authorization activity on 
 *        at the first entrance. There are follow options there:
 *        a phone number enter, sms code verification procedure 
 *        
 *        (c) 2014 Evgeny Makarov
 */

//import android.os.Bundle; 
import java.util.Random;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.util.Log;



public class AuthActivity extends Activity
{
	    String phone_num ="", gen_sms_code = "";
	    boolean is_match=false;
	    int zero_length=0,wrong_length=1,wrong_code=2,success=3;

		protected void onCreate(Bundle savedInstanceState) 
	    {
		    super.onCreate(savedInstanceState);
		    setContentView(R.layout.auth_layout);
		}
	    
	    public void OnAuthClick (View v)
	    {   
	    	EditText ph_num = (EditText) findViewById(R.id.PhoneNumber);
	    	phone_num = ph_num.getText().toString(); 
	    	
	    	if (ph_num.getText().toString().length() == 0)
	    	{
	    		getToast(zero_length);
	    		ph_num.setText("");
	    	}
	    	else if (ph_num.getText().toString().length() != 10)
	    	{
	    		getToast(wrong_length);
	    		ph_num.setText("");
	    	}
	    	else
	    	{
	    	    gen_sms_code = genRandomCode(4);
	    	    ph_num.setText("");
	    	    Log.d("Code",gen_sms_code);
	    	
	    	   /* Create and show the SMS Code entering form to authorize 
	    	    * the subscriber by the sent him SMS code
	    	   */
	    	
	    	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    	    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	
	    	    final View ent_code_layout = inflater.inflate(R.layout.ent_code_layout, null);
	    	
	        	builder.setView(ent_code_layout);
	    	    // Add the buttons
	    	    //DialogInterface code_dialog = new DialogInterface();
	    	    builder.setPositiveButton("OK", 
	    	    	new DialogInterface.OnClickListener() 
	    	    {
	    	           public void onClick(DialogInterface dialog, int id) 
	    	           {
	    	            // User clicked OK button
	    	        	EditText SMSCodeValue = (EditText) ent_code_layout.findViewById(R.id.SMSCodeValue);
	    	   	    	String input_code = SMSCodeValue.getText().toString();
	    	   	    	int length = SMSCodeValue.getText().toString().length();    	        	   
	    	        	   
	    	   	    	   if (length == 4)
	    	   	    	   { 
	    	   	    		     //String ent_sms_code = input_code;
	    	   	    		     if ( !input_code.equalsIgnoreCase(gen_sms_code))
	    	   	    		     {
	    	   	    		    	 getToast(wrong_code);
	    	   	    		     }
	    	   	    		     else
	    	   	    		     {
	    	   	    		    	 getToast(success);
	    	   	    		     }
	    	   	    	   }
	    	   	    	   else
	    	   	    	   {
	    	   	    		     getToast(wrong_length);
	    	   	    	   }

	    	           }
	    	      });
	    	    
	    	      builder.setNegativeButton("Cancel", 
	    	    	new DialogInterface.OnClickListener() 
	    	      {
	    	           public void onClick(DialogInterface dialog, int id) 
	    	           {
	    	               // User cancelled the dialog
	    	        	   dialog.dismiss();
	    	           }
	    	      });

	    	      // Create the AlertDialog
	    	      AlertDialog sms_code_dialog = builder.create();
	    	      sms_code_dialog.show();
	    	      
	    	    	
	    	}
	    }
	    
	    public String genRandomCode (int position)
	    {   
	    	String code="";
	    	Random random = new Random();
	    	int code_int = random.nextInt();
	    	String test = String.valueOf(code_int);
	    	code=test.substring(1,position+1); 		
	    	return code;
	    }
	    
	    protected void getToast(int reason)
	    {
	    	Context context = getApplicationContext();
	    	CharSequence text = "Error!!!";
	    	text = getResString(reason);
	    	int duration = Toast.LENGTH_LONG;
	    	Toast toast = Toast.makeText(context, text, duration);
	    	toast.show();
	    }
	    
	    protected String getResString(int reason)
	    {
	    	String text_res="";
	    	Resources auth_res = getResources();
	    	switch (reason)
	    	{
	    	 case 0:text_res = auth_res.getString(R.string.zero_length_rus);break;
	         case 1:text_res = auth_res.getString(R.string.wrong_length_rus);break;
	         case 2:text_res = auth_res.getString(R.string.wrong_code_rus);break;
	         case 3:text_res = auth_res.getString(R.string.success_code_rus);break;
	    	}
	    	
	    	return text_res;	    	
	    }
	    
	    protected void onDestroy(Bundle savedInstanceState) 
		{
			//TODO		
		}
	    
}


//TODO
//1.Add the SMS sending before the Activation layout 